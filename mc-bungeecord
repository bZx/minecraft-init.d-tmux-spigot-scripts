#!/bin/bash
### BEGIN INIT INFO
# Provides:          mc-bungeecord
# Required-Start:    $remote_fs $network $syslog
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start BungeeCord Minecraft Proxy
### END INIT INFO
# author: bZx
# description: A tmux BungeeCord Server controller init.d script based on CraftThatBlock's one
###############CONFIGURATION#################################

#The user you want the task to run as.
USER="spigot"

#The name of the server (used for the tmux session name).
NAME="bungeecord"

#The script used to start the server.
SCRIPT="/Spigot/BungeeCord/start.sh"

#Logs file.
LOGS=`ls -t -c1 /Spigot/BungeeCord/proxy.log.* | head -1`

#The number of seconds to wait when the server doesn't stop on request.
#In the stop function an alert will be shown if that timeout is reached.
#In force-stop function a KILL signal will be sent to the server if that timeout is reached.
DELAY=20

###############END CONFIGURATION#############################
#You shouldn't need to edit anything too much from here!

function status {
    if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
       echo "$NAME is running."
    else
       echo "$NAME is not running."
    fi
}


function start {
    #Start command
    if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
       echo "$NAME is already running."
    else
       su - $USER -c "tmux new -d -s ${NAME}"
       su - $USER -c "tmux send -t ${NAME} 'sh ${SCRIPT}' ENTER"
       echo "Starting the server ${NAME}"
    fi
}
function force-stop {
    #Force-stop command
    if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
      echo "end command sent to $NAME  ..."
      su - $USER -c "tmux send -t ${NAME} 'end ' ENTER"
      running=true
      curdelay=0
      while [ "$running" = true ]
      do
        if [ "$curdelay" -gt "$DELAY" ]; then
          echo "$NAME is still running after $DELAY seconds."
          echo "Here are the last logs of $NAME:"
          echo "===================================="
          tail -n50 $LOGS
          echo "===================================="
          echo "Killing session."
          su - $USER -c "tmux kill-session -t ${NAME}"
          exit 0
        fi
        curdelay=$((curdelay+1))
        sleep 1
        if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
          running=true
          su - $USER -c "tmux send -t ${NAME} 'exit' ENTER"
        else
          running=false
        fi
      done
      echo "Stopped the server: $NAME"
    else
      echo "$NAME is already shutdown."
    fi
}
function stop {
    #Stop command
    if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
      echo "end command sent to $NAME  ..."
      su - $USER -c "tmux send -t ${NAME} 'end ' ENTER"
      running=true
      curdelay=0
      while [ "$running" = true ]
      do
        if [ "$curdelay" -gt "$DELAY" ]; then
          echo "$NAME is still running after $DELAY seconds."
          echo "Here are the last logs of $NAME:"
          echo "===================================="
          tail -n50 $LOGS
          echo "===================================="
          echo "You can try to stop it again or to kill it."
          echo "Usage: $0 {stop|force-stop}"
          exit 1
        fi
        curdelay=$((curdelay+1))
        sleep 1
        if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
          running=true
          su - $USER -c "tmux send -t ${NAME} 'exit' ENTER"
        else
          running=false
        fi
      done
      echo "Stopped the server: $NAME"
    else
      echo "$NAME is already shutdown."
    fi
}

function attach {
    #Attach session command
    if (su - $USER -c "tmux has-session -t \"${NAME}\" 2> /dev/null"); then
      echo "La session va être attaché dans 7s à ce terminal."
      echo "Vous pouvez la détacher en effectuant la combinaison de touche suivante:";
      echo ">>>>> ctrl + b d <<<<<"
      sleep 7
      su - $USER -c "tmux attach -t ${NAME}"
    else
      echo "$NAME is not running."
      exit 1
    fi
}
function showlogs {
    echo "Quel éditeur voulez-vous utiliser ? Vous pouvez par exemple utiliser 'tail', 'less', 'vim' ou 'nano'."
    read -r -p "Entrez le nom de l'éditeur de fichier à utiliser:" response
    case $response in
       [tT][aA][iI][lL])
         sudo tail -500f $LOGS
       ;;
       *)
         sudo $response $LOGS
       ;;
    esac
}
case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        force-stop
        sleep 1
        start
        echo "Restarted: $NAME"
    ;;
    attach)
        attach
    ;;
    force-stop)
        force-stop
    ;;
    kill)
        force-stop
    ;;
    force-reload)
        force-stop
        sleep 1
        start
        echo "Restarted: $NAME"
    ;;
    logs)
        showlogs
    ;;
    status)
        status
    ;;
    *)
        echo "Usage: $0 {start|stop|restart|attach|kill|status|logs|force-reload}"
        exit 1
    ;;
esac

exit 0
