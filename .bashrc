#Add the following lines to your /root/.bashrc file

#Server List (each name correspond to a /etc/init.d/<mc-server>.sh script)
# You can add multiple spigot servers and bungeecord servers
MCSERVERS=("mc-server1" "mc-server2" "mc-server3")
MCBUNGEECORD=("mc-bungeecord")

#Prepare special constants
MCALLSRV="${MCSERVERS[@]} ${MCBUNGEECORD[@]}"

function getOneMCSrvPerLine() {
        for i in ${MCALLSRV[@]}; do echo $i; done
}

MCPREFIX="$(getOneMCSrvPerLine | sed -e '$!{N;s/^\(.*\).*\n\1.*$/\1\n\1/;D;}')"

#The delay between servers restart when using the restarting without interruption functionnality
DELAYNOINTERRUPT=17

#Minecraft server management functions
function mc-list {
    mc-list-all | grep "+"
}
function mc-list-all {
    service --status-all 2>&1 | grep "${MCPREFIX}"
}
function mc-service {
    if [ $# -lt 2 ]; then
        echo "Usage: ${0} <a[ll]|m[inecraft]|b[ungeecord]> <service parameter (start|stop|restart|attach|status|logs|action|force-stop|force-reload|kill)>"
        return
    fi
    curarray=()
    case "$1" in
        all)
            curarray=${MCALLSRV[@]}
        ;;
        a)
            curarray=${MCALLSRV[@]}
        ;;
        \*)
            curarray=${MCALLSRV[@]}
        ;;
        minecraft)
            curarray=${MCSERVERS[@]}
        ;;
        m)
            curarray=${MCSERVERS[@]}
        ;;
        bungeecord)
            curarray=${MCBUNGEECORD[@]}
        ;;
        b)
            curarray=${MCBUNGEECORD[@]}
        ;;
        *)
            for srv in $MCALLSRV
            do
                if [[ $srv == $1 ]]; then
                    service $srv ${@:2}
                    return
                fi
            done
            echo "Usage: ${0} <a[ll]|m[inecraft]|b[ungeecord]> <service parameter (start|stop|restart|attach|status|logs|action|force-stop|force-reload|kill)>"
            return
        ;;
    esac
    i=0
    echo ${curarray[@]}
    for srv in $curarray; do
        service $srv ${2}
        if [[ $2 == "restart" ]] && [[ $3 == "wait" ]]  && [[ $i -lt ${#curarray[@]} ]]; then
            echo "waiting ${DELAYNOINTERRUPT}s"
            sleep $DELAYNOINTERRUPT
        fi
        i=$((i+1))
    done
}
# Alias
alias mc='mc-service'
#List status of servers
alias mclistall='service --status-all 2>&1 | grep "${MCPREFIX}"'
alias mcla='mclistall'
alias mcstatus='mcla'
alias mclist='mclistall | grep "+"'
alias mcl='mclist'


#Other functions
alias mcaction='mc-service $1 action ${@:2}'
alias mca='mcaction'
alias mcstopall='mc all stop'
alias mcforcestopall='mc all force-stop'
alias mcstartall='mc all start'
alias mcrestartall='mc all restart'
alias mcrestartall-no-interupt='mc minecraft restart wait'
alias mckillall='mc all kill'